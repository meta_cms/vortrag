require "bundler/capistrano"

set :application, "vortrag"
set :bundle_without, [:development, :test, :assets]
set :deploy_to,   "/home/deploy/#{application}"
set :scm,         :git
set :repository,  "git@bitbucket.org:meta_cms/#{application}.git"
set :branch,      "master"
set :user,        'deploy'
set :use_sudo,    false
set :deploy_via,  :remote_cache
set :normalize_asset_timestamps, false
default_run_options[:pty] = true

after 'deploy',                 'deploy:cleanup'
after 'deploy:create_symlink',  'metacms:upload_assets'
before 'deploy:update_code',    'metacms:compress_assets'

server "78.47.60.210", :app, :web, :db, primary: true

namespace :deploy do
  %w[start stop].each do |command|
    desc "#{command} unicorn server"
    task command, roles: :app, except: {no_release: true} do
      run "/etc/init.d/unicorn_#{application} #{command}"
    end
  end
  
  desc "restart unicorn server"
  task :restart, roles: :app, except: {no_release: true} do
    run "/etc/init.d/unicorn_#{application} stop"
    run "/etc/init.d/unicorn_#{application} start"
  end

  task :setup_config, roles: :app do
    sudo "ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-enabled/#{application}"
    sudo "ln -nfs #{current_path}/config/unicorn_init.sh /etc/init.d/unicorn_#{application}"
  end
  after "deploy:setup", "deploy:setup_config"
  after "deploy:setup", "nginx:restart"
end

namespace :nginx do
  desc "restarts nginx"
  task :restart, roles: :app do
    sudo "/etc/init.d/nginx restart"
  end
end

namespace :metacms do
  desc "Creates Bitbucket repro and grants permissions"
  task :bitbucket do
    run_locally "curl --request POST --user meta_cms:kumeta2012 https://api.bitbucket.org/1.0/repositories/ --data name=#{application} --data scm=git"
    run_locally "curl --request PUT --user meta_cms:kumeta2012 https://api.bitbucket.org/1.0/privileges/meta_cms/#{application}/innomind --data write"
    run_locally "curl --request PUT --user meta_cms:kumeta2012 https://api.bitbucket.org/1.0/privileges/meta_cms/#{application}/pwesner --data admin"
    if run_locally("git remote") =~ /origin/
      run_locally "git push"
    else
      run_locally "git remote add origin ssh://git@bitbucket.org/meta_cms/#{application}.git"
      run_locally "git push -u origin master"
    end
  end
  after "deploy:setup", "metacms:bitbucket"
  
  desc "Compress assets in a local file"
  task :compress_assets do
    run_locally("rm -rf public/assets/*")
    run_locally("bundle exec rake assets:precompile")
    run_locally("touch assets.tgz && rm assets.tgz")
    run_locally("tar zcvf assets.tgz public/assets/")
    run_locally("mv assets.tgz public/assets/")
  end

  desc "Upload assets"
  task :upload_assets do
    upload("public/assets/assets.tgz", release_path + '/assets.tgz')
    run "cd #{release_path}; tar zxf assets.tgz; rm assets.tgz"
  end
end

namespace :db do
  desc "Create database yaml in shared path" 
  task :default do
    db_config = ERB.new <<-EOF
    # SQLite version 3.x
    #   gem install sqlite3-ruby (not necessary on OS X Leopard)
    development:
      adapter: sqlite3
      database: #{shared_path}/db/development.sqlite3
      pool: 5
      timeout: 5000

    # Warning: The database defined as "test" will be erased and
    # re-generated from your development database when you run "rake".
    # Do not set this db to the same as development or production.
    test:
      adapter: sqlite3
      database: #{shared_path}/db/test.sqlite3
      pool: 5
      timeout: 5000

    production:
      adapter: postgresql
      database: #{application}_production
      username: innomind_production
      password: Aeghaeke5
      host: localhost
      encoding: UTF8
    EOF

    run "mkdir -p #{shared_path}/db" 
    run "mkdir -p #{shared_path}/config" 
    put db_config.result, "#{shared_path}/config/database.yml" 
  end
  before "deploy:setup", "db"

  desc "creates the database"
  task :create, roles: :db do
    run_rake 'db:create'
  end
#  after "deploy:setup", "db:create"
  
  desc "runs db:seed"
  task :seed, roles: :db do
    run_rake "db:seed"
  end
  
  desc "Make symlink for database yaml" 
  task :symlink do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
  after "deploy:update_code", "db:symlink"
end

def run_rake(task, options={}, &block)
  command = "cd #{latest_release} && bundle exec rake #{task} RAILS_ENV=#{rails_env}"
  run(command, options, &block)
end